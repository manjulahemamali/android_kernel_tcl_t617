/* Begin meng.zhang Double Click Wakeup for task 10011909 on 2020/10/21 */
/*
 *  Generic DT helper functions for touchscreen devices
 *
 *  Copyright (c) 2014 Sebastian Reichel <sre@kernel.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 * history 2: meng.zhang Double Click Wakeup for task 10011909 on 2020/10/21 
 */

#include <tct_tpd.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/input.h>
#include <linux/input/mt.h>
#include <linux/input/touchscreen.h>
#include <linux/module.h>

#ifdef CONFIG_TCT_TOUCHSCREEN_GESTURE_WAKEUP
void (*tct_set_double_wakeup_en)(int enable);
#define GESTURE_ON 1
#define GESTURE_OFF 0
//begin modified by xiongbo.huang for defect 10103612 on 20201122
bool double_wakeup_status = false; //kernel default set enable
//end modified by xiongbo.huang for defect 10103612 on 20201122
static ssize_t double_wakeup_enable_show(struct device *dev,
    struct device_attribute *attr, char *buf) {

    if(double_wakeup_status){
        return snprintf(buf, PAGE_SIZE, "Gesture DoubleClick Wakeup is Enabled\n");
    }else{
        return snprintf(buf, PAGE_SIZE, "Gesture DoubleClick Wakeup is Disabled\n");
    }
}

static ssize_t double_wakeup_enable_store(struct device* dev,
    struct device_attribute *attr, const char *buf, size_t count) {

    unsigned long state;
    ssize_t ret;

    if(!tct_set_double_wakeup_en){
        goto function_err;
    }

    ret = kstrtoul(buf, 2, &state);
    if (ret){
        TCT_ERROR("read gesture_enable file failed\n");
    }

    if(state == GESTURE_ON){
        tct_set_double_wakeup_en(GESTURE_ON);
        double_wakeup_status = true;
    }else if(state == GESTURE_OFF){
        tct_set_double_wakeup_en(GESTURE_OFF);
        double_wakeup_status = false;
    }else{
        TCT_ERROR("invalid cmd for gesture enbale\n");
    }
    return count;
function_err:
    return count;
}

/* Requst  File Access Rights 0664 */
DEVICE_ATTR(double_wakeup_enable, 0664, double_wakeup_enable_show, double_wakeup_enable_store);

static void create_double_wakeup_enable_node(void){

    static struct class *tct_touch_class = NULL;
    static struct device *gesture_dev = NULL;

    TCT_FUNC_ENTER();
    if(!gesture_dev)
    {
        if(!tct_touch_class)
        {
            tct_touch_class = class_create(THIS_MODULE, "tct_touch");
            if (IS_ERR(tct_touch_class))
            {
                TCT_ERROR("Failed to create class(tct_touch!\n");
                goto get_info_err;
            }
        }
        gesture_dev = device_create(tct_touch_class,NULL, 0, NULL, "gesture");
        if (IS_ERR(gesture_dev))
        {
            TCT_ERROR("Failed to create device(gesture_dev)!\n");
            goto get_dev_err;
        }
        if (device_create_file(gesture_dev, &dev_attr_double_wakeup_enable) < 0){
            TCT_ERROR("Failed to create double_wakeup_enable file!\n");
        }
    }

    TCT_FUNC_EXIT();
    return;

get_dev_err:
    class_destroy(tct_touch_class);
get_info_err:
    return;
}
#endif

/*****************************************************************************
*  Name: tct_touchscreen_init
*  Brief: 1. Get dts information
*         2. call tpd_driver_add to add tpd_device_driver
*  Input:
*  Output:
*  Return:
*****************************************************************************/
static int __init tct_touchscreen_init(void)
{
    TCT_FUNC_ENTER();

#ifdef CONFIG_TCT_TOUCHSCREEN_GESTURE_WAKEUP
        create_double_wakeup_enable_node();
#endif

    TCT_FUNC_EXIT();
    return 0;
}

static void __exit tct_touchscreen_exit(void)
{
    TCT_FUNC_ENTER();

    TCT_FUNC_EXIT();
}

late_initcall(tct_touchscreen_init);
module_exit(tct_touchscreen_exit);

MODULE_AUTHOR("TCT HZ BSP Team");
MODULE_DESCRIPTION("TCT Touchscreen Driver");
MODULE_LICENSE("GPL v2");


