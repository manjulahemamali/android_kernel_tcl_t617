/* history 1: update key value to 0xf9 */

#ifndef __TCT_TPD_H
#define __TCT_TPD_H

/*****************************************************************************
* Macro definitions using #define
*****************************************************************************/
#ifdef CONFIG_TCT_TOUCHSCREEN_GESTURE_WAKEUP
#define KEY_DOUBLECLICK_WAKEUP 	 0xf9 //kernel report
#endif


/*****************************************************************************
* DEBUG function define here
*****************************************************************************/
#define TCT_DEBUG_EN 1

#if TCT_DEBUG_EN
#define TCT_DEBUG(fmt, args...) do { \
    printk("[TCT_TP]%s:"fmt"\n", __func__, ##args); \
} while (0)

#define TCT_FUNC_ENTER() do { \
    printk("[TCT_TP]%s: Enter\n", __func__); \
} while (0)

#define TCT_FUNC_EXIT() do { \
    printk("[TCT_TP]%s: Exit(%d)\n", __func__, __LINE__); \
} while (0)
#else /* #if TCT_DEBUG_EN*/
#define TCT_DEBUG(fmt, args...)
#define TCT_FUNC_ENTER()
#define TCT_FUNC_EXIT()
#endif

#define TCT_INFO(fmt, args...) do { \
    printk(KERN_INFO "[TCT_TP/I]%s:"fmt"\n", __func__, ##args); \
} while (0)

#define TCT_ERROR(fmt, args...) do { \
    printk(KERN_ERR "[TCT_TP/E]%s:"fmt"\n", __func__, ##args); \
} while (0)

#endif
