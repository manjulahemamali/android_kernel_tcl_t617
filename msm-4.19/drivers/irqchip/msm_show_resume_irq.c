// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright (c) 2011, 2014-2016, 2018, The Linux Foundation. All rights reserved.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
//Begin added by lanying.he for task 10011976 on 2020/09/25
int msm_show_resume_irq_mask = 1;
//End added by lanying.he for task 10011976 on 2020/09/25
module_param_named(
	debug_mask, msm_show_resume_irq_mask, int, 0664);
