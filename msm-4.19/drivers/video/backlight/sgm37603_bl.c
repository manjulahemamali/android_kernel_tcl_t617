/* Copyright (C) 2019 Tcl Corporation Limited */

//#define DEBUG
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/i2c.h>
#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/sysfs.h>
#include <linux/printk.h>
#include <linux/pm_runtime.h>
#include <linux/leds.h>
#include <linux/delay.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/fb.h>
#include <linux/backlight.h>
#include <linux/leds.h>
#include <linux/workqueue.h>


#define SGM37603_SW_RESET 0x01
#define SGM37603_ENABLE 0x10
#define SGM37603_MODE 0x11
#define SGM37603_BL_LBS 0x1A
#define SGM37603_BL_MBS 0x19
#define SGM37603_FLAG   0x1F
#define SGM37603_CURRENT 0x1B

struct sgm37603_data {
	struct i2c_client *i2c_client;
    struct device *dev;
	struct backlight_device *bl;
	u32 en_gpio;

	int current_brightness;
	u32 max_brightness;
};


static int sgm37603_i2c_write(struct i2c_client *client, u8 cmd, u8 arg)
{
	int result;

	/* write out cmd to blinkm - always / default step */
    pr_debug("sgmicro write slave addr 0x%x \n", client->addr);
    pr_debug("sgmicro write cmd 0x%x data 0x%x \n", cmd, arg);
    result = i2c_smbus_write_byte_data(client, cmd, arg);
	if (result < 0)
		return result;

	return 0;
}

static int sgm37603_i2c_read(struct i2c_client *client, u8 cmd)
{
	int result;
    result = i2c_smbus_read_byte_data(client,cmd);
    pr_debug("sgmicro  read cmd 0x%x data 0x%x \n",cmd,result);

    return result;
}
static int sgm37603_bl_set(struct backlight_device *bl,int brightness)
{
	struct sgm37603_data *data = bl_get_data(bl);
    struct i2c_client *client = data->i2c_client;
	u8 reg_data;
	int result;

	pr_debug("%s: sgm37603 set brightness %d \n", __func__, brightness);

	if (brightness > data->max_brightness)
		brightness = data->max_brightness;

	if (brightness) {
		/* MODIFIED-BEGIN by hongwei.tian, 2019-11-20,BUG-8615261*/
#ifndef CONFIG_TCT_BENGAL_RICHLAND
		brightness = (brightness*3276)/4095;
#endif
		/* MODIFIED-END by hongwei.tian,BUG-8615261*/
		result = sgm37603_i2c_read(client, SGM37603_ENABLE);
		if(!(result & 0x0F))
#ifdef CONFIG_TCT_BENGAL_RICHLAND
			sgm37603_i2c_write(client, SGM37603_ENABLE, 0x1F);
#else
			sgm37603_i2c_write(client, SGM37603_ENABLE, 0x17);
#endif

		reg_data = brightness & 0x0f;
		sgm37603_i2c_write(client, SGM37603_BL_LBS, reg_data);
		reg_data = (brightness >> 4) & 0xff;
		sgm37603_i2c_write(client, SGM37603_BL_MBS, reg_data);
	} else {
		/* code */
		sgm37603_i2c_write(client, SGM37603_ENABLE, 0x10); // MODIFIED by hongwei.tian, 2019-11-05,BUG-8507971
	}

    return 0;
}

static int sgm37603_bl_update_status(struct backlight_device *bl)
{
	int brightness = bl->props.brightness;

	if (bl->props.power != FB_BLANK_UNBLANK)
		brightness = 0;

	if (bl->props.fb_blank != FB_BLANK_UNBLANK)
		brightness = 0;

	return sgm37603_bl_set(bl, brightness);
}

static int sgm37603_bl_get_brightness(struct backlight_device *bl)
{
	struct sgm37603_data *data = bl_get_data(bl);

	return data->current_brightness;
}

/* MODIFIED-BEGIN by hongwei.tian, 2019-11-20,BUG-8615261*/
#ifdef CONFIG_PM_SLEEP
static int sgm37603_i2c_suspend(struct device *dev)
{
	struct sgm37603_data *data  = dev_get_drvdata(dev);

    gpio_direction_output(data->en_gpio, 0);
    msleep(5);

	return 0;
}

static int sgm37603_i2c_resume(struct device *dev)
{
	struct sgm37603_data *data  = dev_get_drvdata(dev);

    gpio_direction_output(data->en_gpio, 1);
    msleep(5);

    /*write init*/
    sgm37603_i2c_write(data->i2c_client,SGM37603_ENABLE,0x10); // MODIFIED by hongwei.tian, 2019-11-21,BUG-8615261
	sgm37603_i2c_write(data->i2c_client,SGM37603_MODE,0x05);
	sgm37603_i2c_write(data->i2c_client,SGM37603_CURRENT,0x00);

	return 0;
}
#endif

static SIMPLE_DEV_PM_OPS(sgm37603_i2c_pm_ops, sgm37603_i2c_suspend,
			sgm37603_i2c_resume);

static int sgm37603_bl_setup(struct backlight_device *bl)
{
	struct sgm37603_data *data = bl_get_data(bl);

    /*enable en_gpio*/
 /*   gpio_direction_output(data->en_gpio, 0);
    msleep(10);
    gpio_direction_output(data->en_gpio, 1);
    msleep(5);*/
    /* MODIFIED-END by hongwei.tian,BUG-8615261*/

    /*write init*/
#ifdef CONFIG_TCT_BENGAL_RICHLAND
	sgm37603_i2c_write(data->i2c_client,SGM37603_ENABLE,0x1F);
#else
    sgm37603_i2c_write(data->i2c_client,SGM37603_ENABLE,0x17);
#endif
	sgm37603_i2c_write(data->i2c_client,SGM37603_MODE,0x05);
	sgm37603_i2c_write(data->i2c_client,SGM37603_CURRENT,0x00);

    msleep(20);

	return 0;
}

static const struct backlight_ops sgm37603_bl_ops = {
	.update_status	= sgm37603_bl_update_status,
	.get_brightness	= sgm37603_bl_get_brightness,
};


static int sgm37603_bl_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
    struct backlight_properties props;
	struct backlight_device *backlight;
	struct sgm37603_data *data;
    struct device_node *np;
	int err;

    dev_info(&client->dev, "sgm37603 backlight probe enter \n");
	data = devm_kzalloc(&client->dev,
			sizeof(struct sgm37603_data), GFP_KERNEL);
	if (!data) {
		err = -ENOMEM;
		return err;
	}

	data->i2c_client = client;
	data->dev = &client->dev;

    np = data->dev->of_node;
    if (np) {
        pr_debug("sgm37603  en gpio \n");

        data->en_gpio = of_get_named_gpio_flags(np, "smg,en-gpio",
                        0, NULL);
        if (data->en_gpio < 0)
        {
            pr_debug("not find sgm37603 en gpio \n");
            return -1;
        }
    }

	data->max_brightness = 4095;
	memset(&props, 0, sizeof(props));
	props.type = BACKLIGHT_RAW;
	props.max_brightness = 4095;
	props.brightness = 920; // MODIFIED by hongwei.tian, 2019-11-20,BUG-8615261
	backlight = devm_backlight_device_register(&client->dev,
				dev_driver_string(&client->dev),
				&client->dev, data, &sgm37603_bl_ops, &props);

	if (IS_ERR(backlight)) {
		dev_err(&client->dev, "failed to register backlight\n");
		return PTR_ERR(backlight);
	}
	data->bl = backlight;

    sgm37603_bl_setup(backlight);

	backlight_update_status(backlight);

	i2c_set_clientdata(client, data);
    dev_info(&client->dev, "sgm37603 prob end \n");
	return 0;

}

static int sgm37603_bl_remove(struct i2c_client *client)
{

	return 0;
}

static const struct of_device_id smg_dt_match[] = {
    {.compatible = "smg,sgm37603", },
    {},
};
static const struct i2c_device_id sgm_id[] = {
    {"sgm37603-blacklight", 0},
    {},
};

static struct i2c_driver sgm37603_driver = {
	.class = I2C_CLASS_HWMON,
	.driver = {
		   .name = "sgm37603-blacklight",
            .owner = THIS_MODULE,
            .of_match_table = of_match_ptr(smg_dt_match),
			.pm	= &sgm37603_i2c_pm_ops, // MODIFIED by hongwei.tian, 2019-11-20,BUG-8615261
		   },
	.probe = sgm37603_bl_probe,
	.remove = sgm37603_bl_remove,
	.id_table = sgm_id,
};


module_i2c_driver(sgm37603_driver);

MODULE_AUTHOR("TCL");
MODULE_DESCRIPTION("sgm37603 backlight driver");
MODULE_LICENSE("GPL");
