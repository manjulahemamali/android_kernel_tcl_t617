#include <linux/types.h>
#include <linux/ip.h>
#include <linux/netfilter.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/icmp.h>
#include <linux/sysctl.h>
#include <net/route.h>
#include <net/ip.h>
#include <linux/bitops.h>
#include <linux/err.h>
#include <linux/version.h>
#include <net/tcp.h>
#include <linux/random.h>
#include <net/sock.h>
#include <net/dst.h>
#include <linux/file.h>
#include <net/tcp_states.h>
#include <linux/netlink.h>
#include <net/sch_generic.h>
#include <net/pkt_sched.h>
#include <net/netfilter/nf_queue.h>
#include <linux/netfilter/xt_state.h>
#include <linux/netfilter/x_tables.h>
#include <linux/netfilter/xt_owner.h>
#include <net/netfilter/nf_conntrack.h>
#include <net/netfilter/nf_conntrack_core.h>
#include <net/netfilter/ipv4/nf_conntrack_ipv4.h>

#define TCT_WIFI_CONNECT    0x20
#define TCT_WIFI_DISCONNECT    0x21
//#define TCT_NETLINK_TCP_TRAFFIC		30

#define tcptraffic_write_lock() 			write_lock_bh(&tcptraffic_lock);
#define tcptraffic_write_unlock()			write_unlock_bh(&tcptraffic_lock);
#define LOG_TCT_TAG "tct_tcp_traffic --- "


static struct ctl_table_header *tct_tcp_traffic_table_hrd;
static	u64 tct_rx_count = 0;
static	u64 tct_tx_count = 0;

static int tct_wifi_connected;
static rwlock_t tcptraffic_lock;
static char ipaddr[16];
static u32 wifiIp;
static DEFINE_MUTEX(tcptraffic_netlink_mutex);
static struct sock *tcp_traffic_sock;

static unsigned int tct_tcp_rx_traffic_calc(void *priv,
				      struct sk_buff *skb,
				      const struct nf_hook_state *state){

	struct iphdr *iph;
	if(!tct_wifi_connected){
		return NF_ACCEPT;
	}
	if((iph = ip_hdr(skb)) != NULL && iph->protocol == IPPROTO_TCP){
		//printk("%s tct_tcp_rx_traffic_calc wifiIp=%u tcp d_addr = %u, ntohl d_addr = %u\n",LOG_TCT_TAG, wifiIp, iph->daddr,ntohl(iph->daddr));
		if(wifiIp==ntohl(iph->daddr)){
		  tct_rx_count ++;
	   }
	}
	return NF_ACCEPT;
}

static unsigned int tct_tcp_tx_traffic_calc(void *priv,
  					struct sk_buff *skb,
  					const struct nf_hook_state *state){

  struct iphdr *iph;
  if(!tct_wifi_connected){
    return NF_ACCEPT;
  }
  if((iph = ip_hdr(skb)) != NULL && iph->protocol == IPPROTO_TCP){
	  //framework ipAddr=3232246612  
	  //#tct_tcp_rx_traffic_calc wifiIp=3232246612, tcp saddr = 1412147392,ntohl saddr = 3232246612
	  //printk("%s tct_tcp_rx_traffic_calc wifiIp=%u, tcp s_addr = %u,ntohl s_addr = %u\n",LOG_TCT_TAG, wifiIp, iph->saddr,ntohl(iph->saddr));
	  if(wifiIp==ntohl(iph->saddr)){
		  tct_tx_count ++;
	  }
  }
  return NF_ACCEPT;
}

static int tct_wifidisconnected(struct nlmsghdr *nlh)
{
  tcptraffic_write_lock();
  if(TCT_WIFI_DISCONNECT == nlh->nlmsg_type){
	  memset(ipaddr,0,sizeof(ipaddr));
	  tct_wifi_connected = false;
	  wifiIp=0;
  }
  printk("%s tct_wifidisconnected:tct_wifi_connected=%d\n",LOG_TCT_TAG,tct_wifi_connected);
  tcptraffic_write_unlock();
  return 0;
}

u32 strTou32(char *str){ 
	u32 ipSum=0;
	u32 factor=1;
	u8 len=strlen(str);
	u8 i;
	for(i=len;i>0;i--){
		ipSum+=((str[i-1]-0x30)*factor);
		factor*=10;
	}
	return ipSum;
}

static int tct_wificonnected(struct nlmsghdr *nlh)
{
	char *p = (char *)NLMSG_DATA(nlh);
	tcptraffic_write_lock();
	if(TCT_WIFI_CONNECT == nlh->nlmsg_type){//wifi connected
		if(p){
			memcpy(ipaddr,p,16);// address
			wifiIp=strTou32(ipaddr);
		}
		tct_wifi_connected = true;
	}
	printk("%s tct_wificonnected:tct_wifi_connected = %d, ipaddr = %s,wifiIp=%u\n",LOG_TCT_TAG,tct_wifi_connected,ipaddr,wifiIp);
	tcptraffic_write_unlock();
	return 0;
}


static int tct_tcptraffic_netlink_rcv_msg(struct sk_buff *skb, struct nlmsghdr *nlh, struct netlink_ext_ack *extack)
{
  int ret = 0; 
  printk("%s tct_tcptraffci_netlink_rcv_msg nlh->nlmsg_type = %d\n",LOG_TCT_TAG, nlh->nlmsg_type);
  switch (nlh->nlmsg_type) {
  case TCT_WIFI_CONNECT:// wifi connect
	  ret = tct_wificonnected(nlh);
	  break;

  case TCT_WIFI_DISCONNECT:// wifi disconnect
	  ret = tct_wifidisconnected(nlh);
	  break;
  default:
	  return -EINVAL;
  }
  return ret;
}

static void tcptraffic_netlink_rcv(struct sk_buff *skb)
{
	mutex_lock(&tcptraffic_netlink_mutex);
	netlink_rcv_skb(skb, &tct_tcptraffic_netlink_rcv_msg);
	mutex_unlock(&tcptraffic_netlink_mutex);
}

static int tcptraffic_netlink_init(void)
{
	struct netlink_kernel_cfg cfg = {
		.input	= tcptraffic_netlink_rcv,
	};

	tcp_traffic_sock = netlink_kernel_create(&init_net, TCT_NETLINK_TCP_TRAFFIC, &cfg);
	return tcp_traffic_sock == NULL ? -ENOMEM : 0;
}

static void tcptraffic_netlink_exit(void)
{
	netlink_kernel_release(tcp_traffic_sock);
	tcp_traffic_sock = NULL;
}

static const struct nf_hook_ops tct_traffic_ops[] = {
	{
		.hook		= tct_tcp_rx_traffic_calc,
		.pf		    = NFPROTO_IPV4,
		.hooknum	= NF_INET_PRE_ROUTING,
		.priority	= NF_IP_PRI_MANGLE + 1,
	},
	{
		.hook		= tct_tcp_tx_traffic_calc,
		.pf		    = NFPROTO_IPV4,
		.hooknum	= NF_INET_POST_ROUTING,
		.priority	= NF_IP_PRI_FILTER + 1,
	},
};

static struct ctl_table tct_tcp_traffic_sysctl_table[] = {
	{
		.procname	= "tct_rx_count",
		.data		= &tct_rx_count,
		.maxlen		= sizeof(u64),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "tct_tx_count",
		.data		= &tct_tx_count,
		.maxlen		= sizeof(u64),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},

	{ }
};

static int tct_tcp_traffic_sysctl_init(void)
{
	tct_tcp_traffic_table_hrd = register_net_sysctl(&init_net, "net/tct_tcp",
		                                          tct_tcp_traffic_sysctl_table);
	return tct_tcp_traffic_table_hrd == NULL ? -ENOMEM : 0;
}

static int __init tct_tcpTraffic_init(void)
{
	int ret = 0;
	rwlock_init(&tcptraffic_lock);
	ret = tcptraffic_netlink_init();
	if (ret < 0) {
		printk("%s can not init tcp traffic netlink\n",LOG_TCT_TAG);
	}

	ret |= tct_tcp_traffic_sysctl_init();
	//hooks
	ret |= nf_register_net_hooks(&init_net, tct_traffic_ops,ARRAY_SIZE(tct_traffic_ops));
	if (ret < 0) {
		printk("tcp_traffic tct tcp traffic  module can not register netfilter ops.\n");
	}
	return ret;
}


static void __exit tct_tcpTraffic_fini(void)
{
	if(tct_tcp_traffic_table_hrd){
		unregister_net_sysctl_table(tct_tcp_traffic_table_hrd);
	}
	tcptraffic_netlink_exit();

	nf_unregister_net_hooks(&init_net, tct_traffic_ops, ARRAY_SIZE(tct_traffic_ops));
}


module_init(tct_tcpTraffic_init);
module_exit(tct_tcpTraffic_fini);




